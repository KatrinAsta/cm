########################### License ###########################################################################################################
# R script for Idea Network Analysis (aka Concept mapping) in R to reshape stacked data into a square symmetric summary matrix.

# Copyright (C) 2017  Daniel McLinden
#
# The program distributed here is shared in the hope that this will be useful,
# but WITHOUT ANY WARRANTY;without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.
# This work is licensed under the GNU GENERAL PUBLIC LICENSE
################################################################################################################################################

####### Do the following #######################################################################################################################
# create a folder in the your C drive called "ideanet"
# create an excel file called "input" and save in folder called "ideanet"
# in the file called "input.xlsx" create a worsheet called "stacked" and put the sorting data in this worksheet in the format as
#    described below.
# delete any persons who have incomplete data
#
# Before running the program be sure excel files are saved and  closed.
#
#### IMPORTANT - check text for apostrohes and quotation marks and remove these, reading the data in with
#                these punctuation marks has been known to cause problems as R tries to inerpret columns and as a result will cause data problems.
#
###### FUNCTION OF THIS PROGRAM ################################################################################################################
# This program will  check for data entry errors and, if none, will create a square and symmetric summary matrix of sort data
#         If errors exist, the program will
#                    1. write an error message to the screen,
#                    2. write out an excel file to document the error and provide data to help the user find the error
#                    3. and then stop.
#         A user can then correct the error in the source data and then rerun this program.
# This program will write out a worksheet with the matrix data to the same input.xlsx workbook you created,
#         this is input to the concept mapping program to create maps.
# This program will write out a worksheet with the labels data to the same input.xlsx workbook you created.
#         this is also input to the concept mapping program to analyzed labels and support the choice of cluster names.
# This program will create a blank worksheet where you can enter the text for the items/ideas.  This is needed for the analysis
################################################################################################################################################

###### NOTES ON FILE AND DATA FORMAT ###########################################################################################################
# This program assumes STACKED SORTING DATA with at least three columns,
# 1. person ID variable,
# 2. text variable for a group name and the
# 3. item number for a statement in that group.
# Data will be in the format below but there may be other variables as well if you are using a sorting program
# that collects other data.  A sorting program I use downloads additional variables.
# If these or other variables exist, then you must copy your data to the input worksheet in the format below.
#
#### IMPORTANT - check character data for apostrohes and quotation marks and remove these, reading the data in with
#          these punctuation marks has been known to cause problems as R tries to inerpret columns.

#### IMPORTANT - add or change your column header names to the following;
# person
# group
# item
#
# for example, the following shows the format for three people sorting 10 statements, columns and rows can be in any order

# person	  group	  item
#
#	person_1	label_A	1
#	person_1	label_A	10
#	person_1	label_A	3
#	person_1	label_A	4
#	person_1	label_B	5
#	person_1	label_B	6
#	person_1	label_C	7
#	person_1	label_C	8
#	person_1	label_C	9
#	person_1	label_C	2
#	person_2	label_D	1
#	person_2	label_D	10
#	person_2	label_D	3
#	person_2	label_D	4
#	person_2	label_E	5
#	person_2	label_E	6
#	person_2	label_E	7
#	person_2	label_F	8
#	person_2	label_F	9
#	person_2	label_F	2
#	person_3	label_G	1
#	person_3	label_G	10
#	person_3	label_G	3
#	person_3	label_H	4
#	person_3	label_H	5
#	person_3	label_H	6
#	person_3	label_H	7
#	person_3	label_H	8
#	person_3	label_H	9
#	person_3	label_H	2
#
###### After running the program ############################################################################################
# In the worksheet in input.xlsx called 'ideas'
# Add the text of each item in the "ideas" worksheet in column B beginning in row 2. Best if you also include the item number
#    with the text in the form of "#. Text" or for example "1. text for item 1"

###################################################################################################################################

# rm(list=ls()) # clear workspace
# setwd("C:/ideanet") # set working directory to the location of the data source


#' Reshape stacked concept mapping data
#'
#' @param filename Name of input file to be found in working directory. Defaults to 'input.xlsx'.
#' The file must containg a worksheet of stacked data with the following structure:
#'
#' person	  group	  item
#' person_1	label_A	1
#' person_1	label_A	10
#' person_1	label_A	3
#' person_1	label_A	4
#' person_2	label_D	3
#' person_2	label_D	4
#' person_2	label_E	5
#' ...
#'
#' @param ideasSheet Name of ideas-sheet in the input file. If NULL or not provided, a blank ideas sheet will be added to the workbook.
#' @param stackedSheetName Name of worksheet with stacked data. Defaults to 'stacked'
#'
#' @return Adds worksheets to the file filename in the working directory. The added worksheets are:
#' 'matrix' containing an N by N table with no columnnames, which for each pair of items gives the number of times the pair have been sorted into the same group;
#' 'ideas' containg a N by 2 table with column names item and item_text, where item is the item number (from 1 to N) and item_text is the corresponding statement;
#' 'labels' containing a table with 4 columns with column names person, item, group and label_ID, where
#' person is an identification number for the sorter, item is the item number, group is the suggested label and label_ID is a unique identification number for each suggested label.
#'
#' @export
#'
#' @examples
#'
reshapeStacked <- function(filename="input.xlsx",
                            ideasSheet=NULL,
                            stackedSheetName="stacked")
{

     # require(reshape)
     # require(openxlsx)
     # require(reshape2)
     # require(Matrix)
     # require(plyr)
     # require(compare)

     ##### function clear existing data from workbooks
     # check if workboook contains a worksheet named matrix and then delete the old sheet in order to add a new worksheet by the same name
     rem_matrix<-function(){
          wb<-openxlsx::loadWorkbook(file = filename)
          openxlsx::removeWorksheet(wb, sheet = "matrix")
          openxlsx::saveWorkbook(wb,file = filename,overwrite = TRUE)
     }
     # check if workboook contains a worksheet named labels and then delete the old sheet in order to add a new worksheet by the same name
     rem_labels<-function(){
          wb<-openxlsx::loadWorkbook(file = filename)
          openxlsx::removeWorksheet(wb, sheet = "labels")
          openxlsx::saveWorkbook(wb,file = filename,overwrite = TRUE)
     }
     # check if workboook contains a worksheet named ideas and then delete the old sheet in order to add a new worksheet by the same name
     rem_ideas<-function(){
          wb<-openxlsx::loadWorkbook(file = filename)
          openxlsx::removeWorksheet(wb, sheet = "ideas")
          openxlsx::saveWorkbook(wb,file = filename,overwrite = TRUE)
     }
     # delete worksheet from error1.
     rem_error1_err<-function(){
          wberr1<-openxlsx::loadWorkbook(file = "error1.xlsx")
          openxlsx::removeWorksheet(wberr1, sheet = "Error1")
          openxlsx::saveWorkbook(wberr1,file = "error1.xlsx",overwrite = TRUE)
     }
     rem_error1_seq<-function(){
          wb_seq_err1 <- openxlsx::loadWorkbook(file = "item_seq.xlsx")
          openxlsx::removeWorksheet( wb_seq_err1, sheet = "Error1")
          openxlsx::saveWorkbook( wb_seq_err1, file = "item_seq.xlsx",overwrite = TRUE)
     }
     # delete worksheet from error2
     rem_error2_err<-function(){
          wberr2<-openxlsx::loadWorkbook(file = "error2.xlsx")
          openxlsx::removeWorksheet(wberr2, sheet = "Error2")
          openxlsx::saveWorkbook(wberr2,file = "error2.xlsx",overwrite = TRUE)
     }
     rem_error2_seq<-function(){
          wb_seq_err2 <- openxlsx::loadWorkbook(file = "item_seq.xlsx")
          openxlsx::removeWorksheet(wb_seq_err2, sheet = "Error2")
          openxlsx::saveWorkbook(wb_seq_err2, file = "item_seq.xlsx",overwrite = TRUE)
     }

     # check input.xlsx for existing worksheet and delete
     wb <- openxlsx::loadWorkbook(file=filename)

     sheets <- openxlsx::getSheetNames(filename)

     if("matrix" %in% sheets) rem_matrix()   #if sheet name matrix exists in wb then invoke rem_matrix function to delete the worksheet from the workbook.
     if("labels" %in% sheets) rem_labels()   #if sheet name labels exists in wb then invoke rem_labels function to delete the worksheet from the workbook.
     if("ideas" %in% sheets & is.null(ideasSheet)) rem_ideas()     #if sheet name ideas exists in wb then invoke rem_ideas function to delete the worksheet from the workbook.

     # check error1.xlsx file
     if(file.exists(file="error1.xlsx")) rem_error1_err() # if the error1 file exists then remove an existing worksheet
     if(file.exists(file="item_seq.xlsx")) rem_error1_seq()

     # check error2.xlsx file for existing worksheets for first error checking and delete
     if(file.exists(file="error2.xlsx")) rem_error2_err() # if the error1 file exists it then remove an existing worksheet
     if(file.exists(file="item_seq.xlsx")) rem_error2_seq()

     ##### download the raw data file delete the columns and rows not needed, rename the columns a described above
     stacked <- openxlsx::read.xlsx(filename,sheet=stackedSheetName,colNames=TRUE) # read in data, fill=true is because some columns may be blank


     if(any(!(names(stacked) %in% c("person","item","group")))) stop(paste0("Check column names in sheet ",stackedSheetName," in the file ",filename,". Columns should be named person, item, group."))

     stacked$unique <- paste(stacked$person, stacked$group) # create a unique occurence for each group label if two or more people use the same words
     stacked$label_ID <- as.numeric(as.factor(stacked$unique)) # create a variable to enumerate each unique occurrence

     stacked$unique <- NULL  # remove the pasted column, no longer needed

     n_items <- length(unique(stacked$item)) # number of items in the dataset


     #################################start error checking here



     # sort data: sorting is not necessary for analysis but makes visual inspection easier for error checking
     sorted<-with(stacked,(stacked[order(person,item),])) # sort by person then by item

     # add an index for levels of factor, this will be a counter for loops in second error checking code
     sorted$person_factor <- as.integer(as.factor(sorted$person))
     start<-min(sorted$person_factor)
     stop<-max(sorted$person_factor)



     # create an easy to read output if errors occur by deleting variables that are not needed for inspecting accuracy of data input.
     error_check<-sorted
     error_check$label_ID <- NULL
     error_check$group <- NULL
     error_check$person_factor <- NULL

     # Test 1 is not strictly needed since test 2 will do all error checking but is a quick check on on a common data entry error of a missing item or an item entered twice
     # Test 1: Check that each person has the the number of items as specified by the value of the variable n_items
     error1<-function(){

          wbe1 <- openxlsx::createWorkbook()
          openxlsx::addWorksheet(wbe1,sheetName="Error1")
          openxlsx::writeData(wbe1,sheet = "Error1",combine1)
          openxlsx::saveWorkbook(wbe1,"error1.xlsx",overwrite = TRUE)

          wbiseq <- openxlsx::createWorkbook()
          openxlsx::addWorksheet(wbiseq,sheetName="Item sequence")
          openxlsx::writeData(wbiseq,sheet = "Item sequence",error_check)
          openxlsx::saveWorkbook(wbiseq,"item_seq.xlsx",overwrite = TRUE)

          stop
          ('Data for one or more persons contains either fewer entries than expected or too many entries.
          The program will stop,an excel file <error1.xlsx> indicating which sorts have errors has been written to
          the working directory. Look in the file for persons where the value in the error column is FALSE and
          where the sortedn column contains a value different from the number items.
          Another file <item_seq.xlsx> has been written out, this may help to find errors, the items entered for each person are in sequence,
          use this file to help find an error, do NOT correct errors in this file.  Correct errors in your data entry file.
          Correct the error and run the program again to check for additional errors. ')
     }

     # count the number of items sorted by each person
     person_sort <- plyr::ddply(.data=sorted, .variables="person", dplyr::summarize,sortedn = length(item))
     # test if each person has the number of sorted items equal to the standard

     value_items<-plyr::ddply(.data=person_sort, .variables="person", dplyr::summarize,"FALSE=error"=(sortedn==n_items))

     combine1<-merge(person_sort,value_items,by="person")  # merge dataframes

     if(any(combine1==FALSE)) error1() # if dataframe contains FALSE then invoke error1 function.

     ##### Error check
     # Test 2: check that each person has the same sequence of ID numbers for the items entered.
     error2 <- function(){
          wbe2 <- openxlsx::createWorkbook()
          openxlsx::addWorksheet(wbe2,sheetName="Error2")
          openxlsx::writeData(wbe2,sheet = "Error2",combine2)
          openxlsx::saveWorkbook(wbe2,"error2.xlsx",overwrite = TRUE)

          wbiseq <- openxlsx::createWorkbook()
          openxlsx::addWorksheet(wbiseq,sheetName="Item sequence")
          openxlsx::writeData(wbiseq,sheet = "Item sequence",error_check)
          openxlsx::saveWorkbook(wbiseq,"item_seq.xlsx",overwrite = TRUE)

          stop
          ("Data for one person contains a duplicate value or a value out of sequence for the item numbers. The program will stop,an excel file <error2.xlsx> with the ID of the person has has been written to the working directory. Check the column named as status for errors and then locate that person ID in your data entry file. Another file <item_seq.xlsx> has been written out, this may help to find errors, the items entered for each person are in sequence, use this file to help find an error, do NOT correct errors in this file. Correct errors in your data entry file. Correct the error and run the program again to check for additional errors." )
     }

     sequence <- data.frame("seq_item"=1:n_items) # the sequence of numbers in the sorted data should be be in this order, this is the standard
     # XXXXX Should this be sort(unique(sorted$item)) i stället för 1:n_items ?

     sequence$seq_item <- as.integer(sequence$seq_item) # XXXXX is this really needed?

     ##### for the following loop
     combine2 <- data.frame(matrix("",nrow = stop, ncol = 2,dimnames=list(NULL,c("person","status"))),stringsAsFactors = FALSE) # create an empty dataframe to hold results dimension nrow=number of sorters

     # create a loop from start to stop
     for (count in start:stop){
          sub_item <- subset(sorted, sorted$person_factor==count) # select a subset of item ID numbers based on a perons factor index number
          sub_item$item <- as.integer(sub_item$item) # coerce to integer to ensure all data comparisons are between the same types
          comparison <- compare::compareIdentical(sequence$seq_item, sub_item$item, ignoreOrder=TRUE, coerce=TRUE) # test if the sequences are the same
          row1 <- sub_item[count,] # obtain values for one row of subsetted data

          # row1 <- data.frame(lapply(row1, as.character), stringsAsFactors=FALSE) # format the dateframe to print out text as text NOT as factors
          # XXXXX is this really needed?

          row1$status <- ifelse(comparison$result==FALSE,'ERROR','OK')  # create a character to indicate result of test and add to row1
          combine2[count,1:2] <- row1[,c("person","status")] # add a row for each person with the status of the test for each person
     }

     if(any(combine2=='ERROR')) error2() # if dataframe contains false then invoke error2 function.
     ### end error check of data entry

     ##### if no errors, create the matrix
     # Create an adjency matrix from an incidence matrix, two mode to one mode
     # see https://solomonmessing.wordpress.com/2012/09/30/working-with-bipartiteaffiliation-network-data-in-r/
     # create a recangular sparse matrix

     sparse_mat <-Matrix::spMatrix(nrow=n_items,
                           ncol=length(unique(stacked$label_ID)),
                           i = as.numeric(factor(stacked$item)), # coerce factor to numeric and specify as row index
                           j = as.numeric(factor(stacked$label_ID)), # coerce factor to numeric and specify as col index
                           x = rep(1,length(as.numeric(stacked$item)))) # matrix entries = 1 based on an item in a group.
     rownames(sparse_mat) <- levels(factor(stacked$item))  #assign rowname
     colnames(sparse_mat) <- levels(factor(stacked$label_ID))    # assign colnames

     #create a one mode square and symmetric matrix
     sum_matrix <- Matrix::tcrossprod(sparse_mat) # To get the one-mode representation of ties between rows,multiply the matrix by its transpose.

     #####  error check 3  - matrix is symmetric
     if(!Matrix::isSymmetric(sum_matrix)){
          stop("matrix is not symmetric") # test that matrix is symmetric
          # XXXXX Is this needed? Surely A*t(A) is always symmetrix?
     }

     ##### error check 4 - matrix is square
     matrixrow<-nrow(sum_matrix)
     matrixcol<-ncol(sum_matrix)

     if (matrixrow!=matrixcol){
          stop("matrix is not square") # test if the matrix is square
          # XXXXX Is this needed? Surely A*t(A) is always suqare?
     }

     # if no errors in the matrix then write the result to an excel xlsx file
     sum_matrix_bin <- suppressMessages(Matrix::formatSparseM(sum_matrix, zero.print="0")) # replace structural zeroes with 0
     class(sum_matrix_bin) <- "integer" # change characters to integers
     sum_matrix_bin<-as.data.frame(sum_matrix_bin)

     # write out a xlsx for excel, no row names, no col names
     wb <- openxlsx::loadWorkbook(file=filename)
     openxlsx::addWorksheet(wb,sheetName = "matrix")
     openxlsx::writeData(wb,sheet = "matrix",sum_matrix_bin,colNames = FALSE,rowNames = FALSE)

     # write out labels for label analysis
     openxlsx::addWorksheet(wb,sheetName = "labels")
     openxlsx::writeData(wb,sheet = "labels",stacked,rowNames = FALSE)

     openxlsx::saveWorkbook(wb,filename,overwrite = TRUE)

     # write.xlsx(stacked,file=filename, sheetName="labels", row.names=FALSE, append=TRUE)


     if(is.null(ideasSheet)){
          ########### write out a blank workbook formatted with the ideas worksheet.
          # write out a xlsx for excel ideas worksheet
          dfideas <- as.data.frame(matrix(nrow=n_items,ncol=3,dimnames=list(NULL,c("item","item_text","Directions"))))
          dfideas$item<-1:n_items
          dfideas[1,3]<-"In column B, beginning in row 2, put the text for each item corresponding to the item number in column A.  Ideally in the form of the item number, period and item text (e.g., #. text).  Do not change column A"
          dfideas[is.na(dfideas)]<-"" # set all cells without data to empty/blank
          # write.xlsx(dfideas,file=filename,sheetName="ideas",col.names=TRUE, row.names=FALSE, append=TRUE)

          # write out labels for label analysis
          openxlsx::addWorksheet(wb,sheetName = "ideas")
          openxlsx::writeData(wb,sheet = "ideas",dfideas,rowNames = FALSE)

          openxlsx::saveWorkbook(wb,filename,overwrite = TRUE)
     }
     # End
}

